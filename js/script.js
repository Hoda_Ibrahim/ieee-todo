
var ul = document.getElementById('todos'),
    input = document.getElementById('newTodo');
input.addEventListener("keyup", function() {
    if (event.keyCode === 13) {
        event.preventDefault();
        var newTodo = document.getElementById('newTodo').value,
            li = document.createElement('li'),
            node = document.createTextNode(newTodo),
            removeTodo = document.createElement('button');
        document.getElementById('newTodo').value = '';

        if (newTodo.split(' ').join('').length === 0) {
            alert('no input');
            return false;
        }

        removeTodo.className = 'removeTodo';
        removeTodo.innerHTML = ' Delete';
        removeTodo.setAttribute('onclick', 'removeTodo(this);');

        li.setAttribute('onclick', 'completeTodo(this);');

        li.appendChild(node);
        li.appendChild(removeTodo);
        ul.appendChild(li);
        window.localStorage.items = ul.innerHTML;
    }
});

function getValues() {
    var storedValues = window.localStorage.items;
    if(!storedValues) {
        ul.innerHTML = '<li>Make a to do list</li>';
    }
    else {
        ul.innerHTML = storedValues;
    }
}
getValues();
function removeTodo(item) {
    var parent = item.parentElement;
    parent.parentElement.removeChild(parent);
    window.localStorage.items = ul.innerHTML;
}
function completeTodo(item) {
    item.className = "checked"
    window.localStorage.items = ul.innerHTML;
}


